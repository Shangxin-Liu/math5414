# define variables
VPATH   = ./
HDRDIR  = ./include
LIBDIR	= ./lib
# adjust this for your system

# set options for this machine
# specify which compilers to use for c, fortran and linking
CC	= gcc
LD	= gcc

# compiler flags to be used (set to compile with debugging on)
CFLAGS = -I$(HDRDIR) -O3

# link flags to be used 
LDFLAGS	= -I$(HDRDIR) -L. -L./$(LIBDIR) -O3

# libraries to be linked in
LIBS	=  -lm 

# types of files we are going to construct rules for
.SUFFIXES: .c 

# rule for .c files
.c.o:
	$(CC) $(CFLAGS) -o $*.o -c $*.c

# list of objects to be compiled
OBJS    = \
	src/main.o\
	src/meshReader2D.o\
	src/meshPrint2D.o

main:$(OBJS) 
	$(LD)  $(LDFLAGS) -o main $(OBJS) $(LIBS)
	rm -r $(OBJS)

# what to do if user types "make clean"
clean :
	rm -r $(OBJS)


