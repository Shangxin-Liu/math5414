#include "mesh2D.h"

int main(int argc, char **argv){

  mesh2D *mesh;

  mesh = meshReader2D(argv[1]);
  
  meshPrint2D(mesh);

  return 0;
}
