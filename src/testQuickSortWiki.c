

#include <stdlib.h>
#include <stdio.h>

#include "quickSortWiki.h"

// test example
int compare(void *a, void *b){
  int *pta = (int*) a;
  int *ptb = (int*) b;
  int ia = *pta;
  int ib = *ptb;
  if(ia<ib) return -1;
  if(ia>ib) return +1;
  return 0;
}

int main(int argc, char **argv) 
{
  int N = 100;
  int i;
  int *a = (int*) calloc(N, sizeof(int));
  for(i=0;i<N;++i)
    a[i] = N*drand48();

  printf("\n\nUnsorted array is:  ");
  for(i = 0; i < N; ++i)
    printf(" %d ", a[i]);

  quickSortWiki(a, N, sizeof(int), compare);

  printf("\n\nSorted array is:    ");
  for(i = 0; i < N; ++i)
    printf(" %d ", a[i]);
  printf("\n");

  exit(0);
  return 0;

}

